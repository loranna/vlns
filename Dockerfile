# build stage
FROM golang:1.13 AS build
ADD go.mod /app/
WORKDIR /app

ARG GOPROXY=http://10.11.12.181:3000
ARG MAJOR
ARG MINOR
ARG COMMITCOUNT

ADD . /app
RUN ./scripts/build.sh

# final stage
FROM ubuntu:18.04
RUN apt-get update && apt-get upgrade -y && apt-get install ca-certificates -y
COPY server.crt /
COPY server.key /
COPY --from=build /app/vlns /vlns
ENTRYPOINT ["/vlns"]
CMD [ "version" ]