package cmd

import (
	"encoding/json"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

type Cmd struct {
	Cmd string `json:"cmd"`
}

type Gwifstat struct {
	Cmd
	Stat []struct {
		Name string `json:"name"`
		Run  bool   `json:"run"`
		Rx   int    `json:"rx"`
		Tx   int    `json:"tx"`
	} `json:"stat"`
}

type Gwsysinfo struct {
	Cmd
	Uname   Uname     `json:"uname"`
	Argv    string    `json:"argv"`
	Storage []Storage `json:"storage"`
	Free    int64     `json:"free"`
	Size    int64     `json:"size"`
	Loads   []float64 `json:"loads"`
	Uptime  int       `json:"uptime"`
	Cpus    int       `json:"cpus"`
}
type Uname struct {
	Machine string `json:"machine"`
	Name    string `json:"name"`
	Release string `json:"release"`
	Sys     string `json:"sys"`
	Version string `json:"version"`
}
type Storage struct {
	Free   int64  `json:"free"`
	Size   int64  `json:"size"`
	Folder string `json:"folder"`
}

type Gwifip struct {
	Cmd
	IP []IP `json:"ip"`
}
type IP struct {
	Name string `json:"name"`
	Used bool   `json:"used"`
	IP   string `json:"ip"`
}

type Gwmsgs struct {
	Cmd
	Messages []Message `json:"messages"`
}
type Rsig struct {
	Ant   int     `json:"ant"`
	Rssic int     `json:"rssic"`
	Rssis int     `json:"rssis"`
	Snr   float64 `json:"snr"`
}
type Message struct {
	Tmst    uint32    `json:"tmst"`
	Time    time.Time `json:"time"`
	Freq    int       `json:"freq"`
	Sf      int       `json:"sf"`
	Bw      int       `json:"bw"`
	Cr      string    `json:"cr"`
	Brd     int       `json:"brd"`
	Rsig    []Rsig    `json:"rsig"`
	Mic     string    `json:"mic"`
	Mt      int       `json:"mt"`
	Devaddr string    `json:"devaddr"`
	Fctrl   int       `json:"fctrl"`
	Fcnt    int       `json:"fcnt"`
	Port    int       `json:"port"`
	Toa     int       `json:"toa"`
	Len     int       `json:"len"`
	Data    string    `json:"data"`
}

type Rxacks struct {
	Cmd  string   `json:"cmd"`
	Tmst []uint32 `json:"tmst"`
}

type Txpk struct {
	Cmd  string `json:"cmd"`
	Ant  int    `json:"ant"`
	Tmst uint32 `json:"tmst"`
	Freq int    `json:"freq"`
	Powe int    `json:"powe"`
	Modu string `json:"modu"`
	Ipol bool   `json:"ipol"`
	Datr string `json:"datr"`
	Size int    `json:"size"`
	Data string `json:"data"`
}

func respond(conn *websocket.Conn, msgs []Message) error {
	rxacks := Rxacks{Cmd: "rxack"}
	rxacks.Tmst = make([]uint32, len(msgs))
	for i, msg := range msgs {
		rxacks.Tmst[i] = msg.Tmst
	}
	b, err := json.Marshal(rxacks)
	if err != nil {
		return err
	}
	err = conn.WriteMessage(websocket.TextMessage, b)
	if err != nil {
		return err
	}
	return nil
}

func handlegw(w http.ResponseWriter, r *http.Request) {
	b, _ := httputil.DumpRequest(r, true)
	logrus.Infof("%s", string(b))
	upgrader := websocket.Upgrader{}
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.Errorf("%s", err)
		return
	}
	defer conn.Close()
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			logrus.Errorf("%s", err)
			return
		}
		cmd := &Cmd{}
		err = json.Unmarshal(message, cmd)
		if err != nil {
			logrus.Errorf("%s", err)
			return
		}
		switch cmd.Cmd {
		case "gwifip":
			rx := &Gwifip{}
			err = json.Unmarshal(message, rx)
			if err != nil {
				logrus.Errorf("%s", err)
				return
			}
			logrus.Infof("%+v", rx)
		case "gwsysinfo":
			gw := &Gwsysinfo{}
			err = json.Unmarshal(message, gw)
			if err != nil {
				logrus.Errorf("%s", err)
				return
			}
			logrus.Infof("%+v", gw)
		case "gwifstat":
			gw := &Gwifstat{}
			err = json.Unmarshal(message, gw)
			if err != nil {
				logrus.Errorf("%s", err)
				return
			}
			logrus.Infof("%+v", gw)
		case "gwmsgs":
			gw := &Gwmsgs{}
			err = json.Unmarshal(message, gw)
			if err != nil {
				logrus.Errorf("%s", err)
				return
			}
			logrus.Infof("%+v", gw)
			err = respond(conn, gw.Messages)
			if err != nil {
				logrus.Errorf("%s", err)
				return
			}
		default:
			logrus.Printf("unknown cmd %s in message %s", cmd.Cmd, string(message))
		}
	}
}

func genpayload(msg Message) (string, int) {
	//parse.Do
	return "", 0
}

func handleconfig(w http.ResponseWriter, r *http.Request) {
	b, _ := httputil.DumpRequest(r, true)
	logrus.Infof("%s", string(b))
	if r.Method == http.MethodGet {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		w.Write([]byte("{\"cloud\":\"vlns.tkiraly\",\"radio\":{\"radio_0\":{\"enable\":true,\"type\":\"SX1257\",\"rssi_offset\":-166,\"tx_enable\":true,\"freq\":867500000},\"radio_1\":{\"enable\":true,\"type\":\"SX1257\",\"rssi_offset\":-166,\"tx_enable\":false,\"freq\":868500000},\"clksrc\":1,\"lorawan_public\":true,\"tx_lut_0\":{\"pa_gain\":0,\"mix_gain\":8,\"rf_power\":-6,\"dig_gain\":0},\"tx_lut_1\":{\"pa_gain\":0,\"mix_gain\":10,\"rf_power\":-3,\"dig_gain\":0},\"tx_lut_2\":{\"pa_gain\":0,\"mix_gain\":12,\"rf_power\":0,\"dig_gain\":0},\"tx_lut_3\":{\"pa_gain\":1,\"mix_gain\":8,\"rf_power\":3,\"dig_gain\":0},\"tx_lut_4\":{\"pa_gain\":1,\"mix_gain\":10,\"rf_power\":6,\"dig_gain\":0},\"tx_lut_5\":{\"pa_gain\":1,\"mix_gain\":12,\"rf_power\":10,\"dig_gain\":0},\"tx_lut_6\":{\"pa_gain\":1,\"mix_gain\":13,\"rf_power\":11,\"dig_gain\":0},\"tx_lut_7\":{\"pa_gain\":2,\"mix_gain\":9,\"rf_power\":12,\"dig_gain\":0},\"tx_lut_8\":{\"pa_gain\":1,\"mix_gain\":15,\"rf_power\":13,\"dig_gain\":0},\"tx_lut_9\":{\"pa_gain\":2,\"mix_gain\":10,\"rf_power\":14,\"dig_gain\":0},\"tx_lut_10\":{\"pa_gain\":2,\"mix_gain\":11,\"rf_power\":16,\"dig_gain\":0},\"tx_lut_11\":{\"pa_gain\":3,\"mix_gain\":9,\"rf_power\":20,\"dig_gain\":0},\"tx_lut_12\":{\"pa_gain\":3,\"mix_gain\":10,\"rf_power\":23,\"dig_gain\":0},\"tx_lut_13\":{\"pa_gain\":3,\"mix_gain\":11,\"rf_power\":25,\"dig_gain\":0},\"tx_lut_14\":{\"pa_gain\":3,\"mix_gain\":12,\"rf_power\":26,\"dig_gain\":0},\"tx_lut_15\":{\"pa_gain\":3,\"mix_gain\":14,\"rf_power\":27,\"dig_gain\":0},\"chan_multiSF_0\":{\"enable\":true,\"radio\":1,\"if\":-400000},\"chan_multiSF_1\":{\"enable\":true,\"radio\":1,\"if\":-200000},\"chan_multiSF_2\":{\"enable\":true,\"radio\":1,\"if\":0},\"chan_multiSF_3\":{\"enable\":true,\"radio\":0,\"if\":-400000},\"chan_multiSF_4\":{\"enable\":true,\"radio\":0,\"if\":-200000},\"chan_multiSF_5\":{\"enable\":true,\"radio\":0,\"if\":0},\"chan_multiSF_6\":{\"enable\":true,\"radio\":0,\"if\":200000},\"chan_multiSF_7\":{\"enable\":true,\"radio\":0,\"if\":400000},\"chan_Lora_std\":{\"enable\":true,\"radio\":1,\"if\":-200000,\"bandwidth\":250000,\"spread_factor\":7},\"chan_FSK\":{\"enable\":true,\"radio\":1,\"if\":300000,\"bandwidth\":125000,\"datarate\":50000}}}"))
	} else if r.Method == http.MethodPost {
		w.WriteHeader(http.StatusNotFound)
	}
}

func starthttpserver(err chan<- error) {
	http.HandleFunc("/gateway/02420AFFFF000008", handleconfig)
	http.HandleFunc("/1/websocket", handlegw)
	e := http.ListenAndServeTLS(":443", "server.crt", "server.key", nil)
	err <- e
}

type DevicesFile []Device
type Device struct {
	Deveui  string `json:"deveui"`
	Devaddr string `json:"devaddr"`
	Nwkskey string `json:"nwkskey"`
	Appskey string `json:"appskey"`
}

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "starts app out",
	RunE: func(cmd *cobra.Command, args []string) error {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		errorChannel := make(chan error)

		go starthttpserver(errorChannel)

		select {
		case <-c:
		case e := <-errorChannel:
			return e
		}
		return nil
	},
}
