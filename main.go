package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/loranna/vlns/cmd"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	viper.SetEnvPrefix("LORANNA_VLNS")
	viper.AutomaticEnv()
	cmd.Execute()
}
