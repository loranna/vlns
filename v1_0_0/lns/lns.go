package lns

/*
import (
	"encoding/hex"

	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/lns/radio"
	"gitlab.com/loranna/loranna/v1_0_0/create"
	"gitlab.com/loranna/loranna/v1_0_0/parse"
	"gitlab.com/loranna/vcore/uplink"
)

type LNS struct {
	Devices map[string]Device
	sender  SendReceiver
}

type SendReceiver interface {
	Send(frequency uint32, radio radio.Parameters, payload []byte, deveui string) error
	Receive() (<-chan uplink.WithMetadataFromGateway, error)
}

type Device struct {
	Deveui                        [8]byte
	Devaddr                       [4]byte
	Adr, Adrackreq, Ack, FPending bool
	FcntUp, FcntDown              uint32
	Nwkskey, Appskey              [16]byte
	txpower                       int
	LastRadio                     radio.Parameters
	LastFrequency                 uint32

	otaainprogress bool
}

func (lns *LNS) Downlink(confirmed bool, fport *uint8, frmpayload []byte) error {
	fos := make([]byte, 0)
	down, err := create.Downlink(confirmed, dev.Devaddr, dev.Adr, dev.Adrackreq, dev.Ack, dev.FPending, uint16(dev.FcntUp), fos, fport, frmpayload, nil)
	if err != nil {
		return err
	}
	downe, err := down.Encrypt(dev.Nwkskey, dev.Appskey, byte(dev.FcntUp>>16), byte(dev.FcntUp>>24))
	if err != nil {
		return err
	}
	freq, bw, sf := dev.GetLoRaChannel()
	payload := downe.Bytes()
	r, err := radio.CreateLoRaWAN(sf, bw, dev.txpower)
	if err != nil {
		return err
	}
	err = dev.sender.Send(freq, *r, payload, hex.EncodeToString(dev.Deveui[:]))
	if err != nil {
		return err
	}
	dev.FcntDown++
	return nil
}
func (lns *LNS) Uplink(uplinks <-chan uplink.WithMetadataFromGateway) {
	go lns.monitoruplinks(uplinks)
}

func (lns *LNS) monitoruplinks(uplinks <-chan uplink.WithMetadataFromGateway) {

	for uplink := range uplinks {
		logrus.Debugf("uplink received with payload: %s", uplink.GetPayload())
		dlbytes, err := hex.DecodeString(uplink.GetPayload())
		if err != nil {
			logrus.Warnf("could not parse payload as hex string: %s", uplink.GetPayload())
			continue
		}
		if dev.otaainprogress {

		} else {
			up, err := parse.UplinkEncrypted(dlbytes, dev.Nwkskey, dev.Appskey, byte(dev.FcntUp>>24), byte(dev.FcntUp>>16))
			if err != nil {
				logrus.Debugf("could not parse payload as valid uplink string: %s", uplink.GetPayload())
				continue
			}
			if up.Fcnt() > uint16(dev.FcntDown) {
				dev.FcntDown |= uint32(up.Fcnt())
			} else {
				logrus.Debugf("current counter is higher than received. Current: %d, Received: %d", dev.FcntUp, up.Fcnt())
				continue
			}
			if up.Fport() != nil {
				logrus.Debugf("downlink with fport: %d, payload: %X received", *up.Fport(), up.Frmpayload())
			} else {
				logrus.Debugf("empty uplink received")
			}
			dev.LastFrequency = uplink.GetFrequency()
			dev.LastRadio.Modulation = radio.Modulation_LORA
			dev.LastRadio.Bandwidth = radio.Bandwidth_125KHZ
			dev.LastRadio.SpreadingFactor = radio.SF2SF(uplink.GetSpreadingfactor())
			dev.LastRadio.CodeRate = radio.CodeRate_4_5
		}
	}

}

func (lns *Device) GetLoRaChannel() (freq uint32, bw radio.Bandwidth, sf radio.SpreadingFactor) {
	freq = dev.LastFrequency
	bw = dev.LastRadio.Bandwidth
	sf = dev.LastRadio.SpreadingFactor
	return
}

func Create(sr SendReceiver) (*LNS, error) {
	d := LNS{
		Devices: make(map[string]Device),
		sender:  sr,
	}
	ch, err := d.sender.Receive()
	if err != nil {
		return nil, err
	}
	d.Uplink(ch)
	return &d, nil
}

func (lns *LNS) AddDevice(Deveui [8]byte, Devaddr [4]byte, Nwkskey [16]byte, Appskey [16]byte) error {
	d := Device{}
	copy(d.Deveui[:], Deveui[:])
	copy(d.Devaddr[:], Devaddr[:])
	copy(d.Nwkskey[:], Nwkskey[:])
	copy(d.Appskey[:], Appskey[:])
	lns.Devices[hex.EncodeToString(Deveui[:])] = d
	return nil
}
*/
