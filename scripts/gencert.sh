#!/bin/bash

openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=HU/ST=Budapest/L=Budapest/O=Tkiraly/CN=vlns.tkiraly" -keyout server.key -out server.crt