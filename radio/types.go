package radio

import (
	"fmt"

	"gitlab.com/loranna/vcore/uplink"
)

//go:generate stringer -type=Modulation
//go:generate stringer -type=Bandwidth
//go:generate stringer -type=SpreadingFactor
//go:generate stringer -type=CodeRate

type Modulation uint32

const (
	Modulation_Undefined Modulation = iota
	Modulation_LORA
	Modulation_FSK
)

type Bandwidth uint32

const (
	Bandwidth_Undefined Bandwidth = iota
	Bandwidth_500KHZ
	Bandwidth_250KHZ
	Bandwidth_125KHZ
	Bandwidth_62K5HZ
	Bandwidth_31K2HZ
	Bandwidth_15K6HZ
	Bandwidth_7K8HZ
)

type SpreadingFactor uint32

const (
	SpreadingFactor_Undefined SpreadingFactor = iota
	SpreadingFactor_7
	SpreadingFactor_8
	SpreadingFactor_9
	SpreadingFactor_10
	SpreadingFactor_11
	SpreadingFactor_12
)

type CodeRate uint32

const (
	CodeRate_Undefined CodeRate = iota
	CodeRate_4_5
	CodeRate_4_6
	CodeRate_4_7
	CodeRate_4_8
)

type Parameters struct {
	Modulation
	Bandwidth
	SpreadingFactor
	CodeRate
	Txpower int
}

func CreateParameters(sf SpreadingFactor, bw Bandwidth, txpower int, modulation Modulation) (*Parameters, error) {
	p := &Parameters{}
	p.Modulation = modulation
	if bw != Bandwidth_125KHZ && bw != Bandwidth_250KHZ && bw != Bandwidth_500KHZ {
		return nil, fmt.Errorf("invalid bandwidth: %s", bw)
	}
	p.Bandwidth = bw
	p.CodeRate = CodeRate_4_5
	p.SpreadingFactor = sf
	p.Txpower = txpower
	return p, nil
}

func SF2SF(sf uplink.SpreadingFactor) SpreadingFactor {
	switch sf {
	case uplink.SpreadingFactor_SF7:
		return SpreadingFactor_7

	case uplink.SpreadingFactor_SF8:
		return SpreadingFactor_8

	case uplink.SpreadingFactor_SF9:
		return SpreadingFactor_9

	case uplink.SpreadingFactor_SF10:
		return SpreadingFactor_10

	case uplink.SpreadingFactor_SF11:
		return SpreadingFactor_11

	case uplink.SpreadingFactor_SF12:
		return SpreadingFactor_12
	default:
		return SpreadingFactor_Undefined

	}
}
