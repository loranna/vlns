module gitlab.com/loranna/vlns

go 1.13

replace gitlab.com/loranna/lns => ../lns

require (
	github.com/gorilla/websocket v1.4.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.5.0
)
